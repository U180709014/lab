
public class Main {
	public static void main(String[] args) {
		
		Point testPoint = new Point(10, 10);
		Rectangle testRectangle = new Rectangle(3, 5, testPoint);
		System.out.println(testRectangle.area());
		System.out.println(testRectangle.perimeter());
		
		for (int i = 0; i < testRectangle.corners().length; i++) {
			System.out.println("(" + testRectangle.corners()[i].xCoord + ", " + testRectangle.corners()[i].yCoord + ")");
		}
		
		Circle daire = new Circle(10, new Point(3,3));
		System.out.println(daire.area());
		System.out.println(daire.perimeter());
		
		Circle daire2 = new Circle(5, new Point(2,5));
		System.out.println(daire.intersect(daire2));
	}
}
