
public class Circle {
	int radius;
	Point center;
	
	public Circle(int radius, Point center) {
		this.radius = radius;
		this.center = center;
	}
	
	public double area() {
		return 3.14 * radius * radius;
	}
	
	public double perimeter() {
		return 2 * 3.14 * radius;
	}
	
	public boolean intersect(Circle circle) {
		int sumOfRad = (this.radius + circle.radius) * (this.radius + circle.radius);
		double uzaklık = ((this.center.xCoord) - (circle.center.xCoord)) * ((this.center.xCoord) - (circle.center.xCoord)) + ((this.center.yCoord) - (circle.center.yCoord)) * ((this.center.yCoord) - (circle.center.yCoord));
		if (sumOfRad < uzaklık) {
			return true;
		} else {
			return false;
		}
	}
}
