public class MyTime {
    int hour;
    int minute;

    public MyTime(int hour, int minute) {
        this.hour = hour;
        this.minute = minute;
    }

    public String toString() {
        return (this.hour < 10 ? "0" : "" + this.hour) + ":" + (this.minute < 10 ? "0" : "" + this.minute);
    }

    public int incrementHour(int i) {
        int extraDays = 0;
        if (this.hour + i >= 24) {
            extraDays = (this.hour + i) / 24;
            this.hour = (this.hour + i) % 24;
        }
        return extraDays;
    }

    public int decrementHour(int i) {
        int extraDays = 0;
        if (this.hour - i >= 24) {
            extraDays = (this.hour - i) / 24;
            this.hour = (this.hour - 1) % 24;
        }
        return -extraDays;
    }

    public int incrementMinute(int i) {
        int extraHours = 0;
        if (this.minute + i >= 60) {
            extraHours = (this.minute + i) / 60;
            this.minute = (this.minute + i) % 60;
        }
        return extraHours;
    }

    public int decrementMinute(int i) {
        int extraHours = 0;
        if (this.minute - i >= 60) {
            extraHours = (this.minute - i) / 60;
            this.minute = (this.minute - i) % 60;
        }
        return -extraHours;
    }

    public boolean isBefore(MyTime anotherTime) {
        int a = Integer.parseInt(toString().replaceAll(":", ""));
        int b = Integer.parseInt(anotherTime.toString().replaceAll("-", ""));
        return a < b;
    }
    
    public boolean isAfter(MyTime anotherTime) {
    	int a = Integer.parseInt(toString().replaceAll(":", ""));
        int b = Integer.parseInt(anotherTime.toString().replaceAll("-", ""));
        return a > b;
    }
    
    //public int timeDifference(MyTime anotherTime) {
    	//int hourDiff = 0;
    	//int minDiff = 0;
    	//if (this.isBefore(anotherTime)) {
    		//while(this.isBefore(anotherTime)) {
    			
    		//}
    	//}
    //}
}
