public class MyDateTime {

    public boolean inLeapYear() {
        return this.date.year % 4 == 0 ? true : false;
    }

    MyDate date;
    MyTime time;

    int[] maxDays = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public MyDateTime(MyDate date, MyTime time) {
        this.date = date;
        this.time = time;
    }

    public String toString() {
        return this.date.toString() + " " + this.time.toString();
    }

    public void incrementDay(int i) {
        this.date.incrementDay(i);
    }

    public void incrementDay() {
        this.date.incrementDay();
    }

    public void decrementDay(int i) {
        this.date.decrementDay(i);
    }

    public void decrementDay() {
        this.date.decrementDay();
    }

    public void incrementMonth(int i) {
        this.date.incrementMonth(i);
    }

    public void incrementMonth() {
        this.date.incrementMonth(1);

    }

    public void decrementMonth(int i) {
        this.date.decrementMonth(i);
    }

    public void decrementMonth() {
        this.date.decrementMonth(1);

    }

    public void incrementHour(int i) {
        this.date.incrementDay(this.time.incrementHour(i));

    }

    public void decrementHour(int i) {
        this.date.decrementDay(this.time.decrementHour(i));
    }

    public void incrementHour() {
        this.incrementHour(1);
    }

    public void decrementHour() {
        this.decrementHour(1);
    }

    public void incrementMinute(int i) {
        this.time.incrementHour(i);
    }

    public void incrementMinute() {
        this.time.incrementHour(1);
    }

    public void decrementMinute(int i) {
        this.time.incrementHour(i);
    }

    public void decrementMinute() {
        this.time.incrementHour(1);
    }

    public void incrementYear(int i) {
        this.date.incrementYear(i);
    }

    public void incrementYear() {
        this.date.incrementYear(1);
    }

    public void decrementYear(int i) {
        this.date.decrementYear(i);
    }

    public void decrementYear() {
        this.date.decrementYear(1);
    }

    public boolean isBefore(MyDateTime anotherDateTime) {
        if (this.date.toString() == anotherDateTime.date.toString()) {
        	return this.time.isBefore(anotherDateTime.time);
        }else {
        	return this.date.isBefore(anotherDateTime.date);
        }
    }
    
    public boolean isAfter(MyDateTime anotherDateTime) {
        if (this.date.toString() == anotherDateTime.date.toString()) {
        	return this.time.isAfter(anotherDateTime.time);
        }else {
        	return this.date.isAfter(anotherDateTime.date);
        }
    }
    
    public String dayTimeDifference(MyDateTime anotherDateTime) {
    	
    }
}