import java.io.IOException;

import java.util.Scanner;


public class TicTacToe {


    public static void main(String[] args) throws IOException {

        Scanner reader = new Scanner(System.in);

        char[][] board = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};

        int moveCount = 0;

        printBoard(board);

        label:
        while (true) {

            int row, col;

            do {

                System.out.print("Player X enter row number:");

                row = reader.nextInt();

                System.out.print("Player X enter column number:");

                col = reader.nextInt();


            } while (!(row > 0 && row < 4 && col > 0 && col < 4 && board[row - 1][col - 1] == ' '));

            board[row - 1][col - 1] = 'X';
            moveCount = moveCount + 1;
            printBoard(board);

            if (checkBoard(board, row, col)) {

                System.out.println(board[row - 1][col - 1] + " wins");

                break label;

            }else if (moveCount == 9) {
                System.out.println("Draw");
                break label;
            }


            do {

                System.out.print("Player O enter row number:");

                row = reader.nextInt();

                System.out.print("Player O enter column number:");

                col = reader.nextInt();


            } while (!(row > 0 && row < 4 && col > 0 && col < 4 && board[row - 1][col - 1] == ' '));

            board[row - 1][col - 1] = 'O';
            moveCount = moveCount + 1;
            printBoard(board);

            if (checkBoard(board, row, col)) {

                System.out.println(board[row - 1][col - 1] + " wins");

                break label;

            }else if (moveCount == 9) {
                System.out.println("Draw");
                break label;
            }



        }

        reader.close();


    }


    public static void printBoard(char[][] board) {

        System.out.println("    1   2   3");

        System.out.println("   -----------");

        for (int row = 0; row < 3; ++row) {

            System.out.print(row + 1 + " ");

            for (int col = 0; col < 3; ++col) {

                System.out.print("|");

                System.out.print(" " + board[row][col] + " ");

                if (col == 2)

                    System.out.print("|");


            }

            System.out.println();

            System.out.println("   -----------");


        }


    }


    public static boolean checkBoard(char[][] board, int row, int col) {

        boolean winFlag = true;

        for (int i = 0; i < 3; i++) {

            if (board[row - 1][i] != board[row - 1][col - 1]) {

                winFlag = false;

                break;

            }

        }

        if (winFlag)
            return true;

        for (int i = 0; i < 3; i++) {

            if (board[i][col - 1] != board[row - 1][col - 1]) {

                winFlag = false;

                break;

            }

        }

        if (winFlag)
            return true;


        if (row + col == 4) {


            if (!(board[2][0] == board[row - 1][col - 1] && board[1][1] == board[row - 1][col - 1] && board[0][2] == board[row - 1][col - 1])) {

                winFlag = false;

            }else {
                winFlag = true;
            }

        }




        if (row == col) {

            if ( !((board[0][0] == board[row - 1][col - 1]) && (board[1][1] == board[row - 1][col - 1]) && (board[2][2] == board[row - 1][col - 1]))) {

                winFlag = false;

            }else {
                winFlag = true;
            }



        }

        if (winFlag)
            return true;

        return winFlag;

    }


}