public class GCDRec{
    public static void main(String[] args){
        
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        int q;
        System.out.println(Gcd(a,b));
        
    }

    public static int Gcd(int a, int b){
        
        if (b == 0){
            return a; 
        }else{
            return Gcd(b, a%b);
        }
    }

}
