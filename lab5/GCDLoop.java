public class GCDLoop{
    public static void main(String[] args){
        
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        int q;
        while(b != 0){
            q = a % b;
            b = a;
            q = b;
        }
        System.out.println(a);
    }
}
