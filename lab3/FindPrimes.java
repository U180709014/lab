public class FindPrimes {

    public static void main(String[] args){
        
        int number = Integer.parseInt(args[0]);
        for (int i = 2; i < number; i++){
            if (IsPrime(i)){
                System.out.print(i + " ");
            }
        }


    }

    public static boolean IsPrime(int n){

        for (int i = 3; i < n; i++){
            if (n == 0 || n == 1){
                return false;
            }else if (n == 2){
                return true;        
            }else if (n % i == 0){
                return false;            
            }
        }
        return true;

    }    

}
