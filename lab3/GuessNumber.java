

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		
		
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it? :");
		
        boolean playing = true;
        int count = 1;

		while (playing){
        int guess = reader.nextInt(); //Read the user input

        if (guess == number) {
            System.out.println("Congratulations!");
            System.out.println("You won in " + count + " tries.");
            playing = false; 
            reader.close(); //Close the resource before exiting
        }else if (guess == -1) {
            System.out.println("Sorry the number was " + number);
            System.out.println("You quit after " + count + " tries.");
            playing = false;
            reader.close(); //Close the resource before exiting
        }else if (guess > number){
            count = count + 1;
            System.out.println("Sorry my number is less than yours.");
            System.out.print("Type -1 or guess again: ");     
        }else if (guess < number) {
            count = count + 1;
            System.out.println("Sorry my number is greater than yours.");
            System.out.print("Type -1 or guess again: ");    
        }
		
		
		
		
	}
	
	}
}
